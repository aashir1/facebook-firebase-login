import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import googleLogin from 'react-facebook-login';
import dbconfig from "./db";
import firebase from 'firebase';
import { List } from './list';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isUser: false,
            userCredential: {}
        }
        this.provider = new firebase.auth.GoogleAuthProvider();

    }

    googleLogin = () => {
        const me = this;
        firebase.auth().signInWithPopup(this.provider).then(function (result) {
            var token = result.credential.accessToken;
            var user = result.user;
            console.log(user);
            me.setState({ isUser: true, userCredential: user.providerData[0] });
        }).catch(function (error) {
            var errorCode = error.code;
            var errorMessage = error.message;
            var email = error.email;
            var credential = error.credential;
        });

    }

    signOut = () => {
        const me = this;
        firebase.auth().signOut().then(function () {
            me.setState({ isUser: false });
        }).catch(function (error) {
            console.log(error.message);
        });
    }


    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo" />
                    <h1 className="App-title">Welcome to React</h1>
                </header>
                <p className="App-intro">
                    <button onClick={this.googleLogin}> Google Login </button>
                    {
                        this.state.isUser ?
                            <button onClick={this.signOut}> Google Signout </button>
                            :
                            null
                    }
                </p>
                <List data={this.state.userCredential} />

            </div>
        );
    }
}

export default App;
