import React from 'react';
let i = 0;
export const List = (props) => {
    return (
        <li key={i++} style={{listStyle: "none"}}>
            <div style={{display: 'flex', justifyContent: 'space-around'}}>
                <span><img width="300" height="300" src={props.data.photoURL} alt="userImage"/></span>
                <span>
                    {props.data.displayName}
                    <br />
                    {props.data.email}
                </span>
            </div>
        </li>
    );
}